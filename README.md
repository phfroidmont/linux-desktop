# Linux Desktop setup
This project installs my custom desktop setup using Ansible.

# Usage
To avoid having to install python dependencies on your machine, a virtualenv can be used with the help of the provided script:

```bash
./helpers/setup_virtualenv.sh && source .virtualenv/bin/activate
```

After that, running the full setup is straight-forward:

```bash
ansible-playbook site.yml --ask-become-pass
```