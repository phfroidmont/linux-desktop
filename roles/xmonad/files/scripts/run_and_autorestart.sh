#!/bin/bash

until ${@:1} && false; do
    echo "${@:1} crashed with exit code $?.  Respawning.." >&2
    sleep 3
done
