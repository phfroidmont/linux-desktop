#!/bin/bash

# Get primary screen resolution
RES=$(xdpyinfo | awk '/dimensions/{print $2}')

# Generate lockscreen picture based on wallpaper if not already done
if [ ! -f ~/.lockscreen.png ]; then
    convert ~/.wallpaper.jpg -resize "$RES""^" -gravity center -extent "$RES" ~/.lockscreen.png
fi

i3lock -e -f -c 000000 -i ~/.lockscreen.png
